var LifeState;
(function (LifeState) {
    LifeState[LifeState["Alive"] = 0] = "Alive";
    LifeState[LifeState["Dead"] = 1] = "Dead";
})(LifeState || (LifeState = {}));
;

var GameGrid = (function () {
    function GameGrid(size, canvas) {
        this.size = size;
        this.canvas = canvas;
        this.rows = [];
        this.cells = [];

        for (var rowNumber = 0; rowNumber < this.size; rowNumber++) {
            this.rows[rowNumber] = new GameRow(rowNumber, this);
        }

        var that = this;
        for (var rowIndex in this.rows) {
            this.rows[rowIndex].getCells().forEach(function (cell) {
                cell.getNeighbours();
                that.cells.push(cell);
            });
        }
    }
    GameGrid.prototype.debug = function () {
        var debug = [];

        this.rows.forEach(function (row) {
            var tmpArray = [];
            row.cells.forEach(function (cell) {
                tmpArray.push(cell.lifeState === 0 /* Alive */ ? 1 : 0);
            });
            debug.push(tmpArray);
        });

        console.log(debug);
    };

    GameGrid.prototype.reinit = function () {
        this.cells.forEach(function (cell) {
            cell.lifeState = 1 /* Dead */;
        });
    };

    GameGrid.prototype.populate = function () {
        this.cells.forEach(function (cell) {
            cell.setRandomLifeState();
        });
    };

    GameGrid.prototype.draw = function () {
        //console.log('Drawing Rows');
        this.canvas.ctx.clearRect(0, 0, this.size * GameCell.SIZE, this.size * GameCell.SIZE);

        for (var rowIndex in this.rows) {
            var row = this.rows[rowIndex];

            row.draw();
        }

        return false;
    };

    GameGrid.prototype.getCellAtPosition = function (col, row) {
        if ((col >= 0 && col <= this.size) && (row >= 0 && row <= this.size - 1)) {
            return this.rows[row].getCellAtPosition(col);
        }
        return null;
    };
    return GameGrid;
})();

var GameRow = (function () {
    function GameRow(index, grid) {
        this.index = index;
        this.grid = grid;
        this.cells = [];

        for (var cellNumber = 0; cellNumber < grid.size; cellNumber++) {
            this.cells.push(new GameCell(cellNumber, this));
        }
    }
    GameRow.prototype.draw = function () {
        for (var cellIndex in this.cells) {
            var cell = this.cells[cellIndex];
            cell.draw();
        }
    };

    GameRow.prototype.getCells = function () {
        return this.cells;
    };

    GameRow.prototype.getCellAtPosition = function (col) {
        if (col >= 0 && col <= this.cells.length)
            return this.cells[col];
        return null;
    };
    return GameRow;
})();

var GameCell = (function () {
    function GameCell(index, row) {
        this.index = index;
        this.row = row;
        this.neighbours = [];
        this.totalLivingNeighbours = 0;
        this.lifeState = 1 /* Dead */;
    }
    GameCell.prototype.setRandomLifeState = function () {
        this.lifeState = Math.random() > 0.5 ? this.setLifeState(0 /* Alive */) : this.setLifeState(1 /* Dead */);
    };

    GameCell.prototype.setLifeState = function (lifeState) {
        this.lifeState = lifeState;
        return this.lifeState;
    };

    GameCell.prototype.toggleLifeState = function () {
        this.lifeState = this.lifeState === 0 /* Alive */ ? 1 /* Dead */ : 0 /* Alive */;
        return this.lifeState;
    };

    GameCell.prototype.draw = function () {
        //console.log('Drawing individual cell');
        if (this.lifeState === 0 /* Alive */) {
            var gridSize = GameCell.SIZE;
            var xPos = this.index * gridSize;
            var yPos = this.row.index * gridSize;
            var grid = this.row.grid;

            grid.canvas.ctx.fillRect(xPos, yPos, GameCell.SIZE, GameCell.SIZE);
        }
    };

    //TODO: Refactor
    GameCell.prototype.getNeighbours = function () {
        if (this.neighbours.length)
            return this.neighbours;

        var neighbours = [];
        var isFirstRow = false;
        var isLastRow = false;
        var isFirstCol = false;
        var isLastCol = false;
        var grid = this.row.grid;
        var gridSize = grid.size;

        isFirstRow = this.row.index === 0 ? true : false;
        isFirstCol = this.index === 0 ? true : false;
        isLastRow = this.row.index === gridSize ? true : false;
        isLastCol = this.index === gridSize ? true : false;

        if (!isFirstRow) {
            neighbours.push(grid.getCellAtPosition(this.index, this.row.index - 1));
            if (!isFirstCol)
                neighbours.push(grid.getCellAtPosition(this.index - 1, this.row.index - 1));
            if (!isLastCol)
                neighbours.push(grid.getCellAtPosition(this.index + 1, this.row.index - 1));
        }

        if (!isLastRow) {
            neighbours.push(grid.getCellAtPosition(this.index, this.row.index + 1));
            if (!isFirstCol)
                neighbours.push(grid.getCellAtPosition(this.index - 1, this.row.index + 1));
            if (!isLastCol)
                neighbours.push(grid.getCellAtPosition(this.index + 1, this.row.index + 1));
        }

        if (!isFirstCol)
            neighbours.push(grid.getCellAtPosition(this.index - 1, this.row.index));
        if (!isLastCol)
            neighbours.push(grid.getCellAtPosition(this.index + 1, this.row.index));

        this.neighbours = neighbours;
        return neighbours;
    };
    GameCell.SIZE = 8;

    GameCell.CANVAS_ID = 'a_canvas';
    return GameCell;
})();

var Game = (function () {
    function Game(size, canvasId, interval) {
        this.canvas = new GameCanvas(canvasId, size);
        this.currentGen = new GameGrid(size, this.canvas);
        this.currentGen.populate();
        this.nextGen = new GameGrid(size, this.canvas);
        this.currentGen.draw();
    }
    Game.prototype.step = function () {
        var that = this;
        this.currentGen.cells.forEach(function (cell) {
            var livingNeighbours = 0;
            cell.neighbours.forEach(function (neighbour) {
                if (neighbour) {
                    if (neighbour.lifeState === 0 /* Alive */)
                        livingNeighbours++;
                }
            });

            var nextGenCell = that.nextGen.getCellAtPosition(cell.index, cell.row.index);
            if (cell.lifeState === 0 /* Alive */) {
                nextGenCell.lifeState = 0 /* Alive */;

                if (livingNeighbours < 2)
                    nextGenCell.lifeState = 1 /* Dead */;
                if (livingNeighbours > 3)
                    nextGenCell.lifeState = 1 /* Dead */;
            } else {
                if (livingNeighbours == 3)
                    nextGenCell.lifeState = 0 /* Alive */;
            }
        });

        this.currentGen.debug();

        //this.currentGen = this.nextGen;
        this.nextGen.cells.forEach(function (nCell) {
            that.currentGen.rows[nCell.row.index].cells[nCell.index].lifeState = nCell.lifeState;
        });

        //this.currentGen.canvas.ctx.clearRect(0, 0, 512, 512);
        this.currentGen.draw();
        this.nextGen.reinit();
    };
    return Game;
})();

var GameCanvas = (function () {
    function GameCanvas(canvasId, gridSize) {
        var canvas = document.getElementById(canvasId);
        canvas.height = gridSize * GameCell.SIZE;
        canvas.width = gridSize * GameCell.SIZE;

        this.ctx = canvas.getContext('2d');
    }
    return GameCanvas;
})();

var cgol = new Game(128, 'a_canvas', 0);

setInterval(function () {
    cgol.step();
}, 100);
