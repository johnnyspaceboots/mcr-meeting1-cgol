 enum LifeState {
	 Alive, 
	 Dead
 };
 

 interface Grid {
	 size: number;
	 rows: Array<GridRow>;
	 draw(): void;
	 getCellAtPosition(col:number, row:number): GridCell;
	 debug();
	 //new(size: number);
 }
 
 interface GridRow {
	 index: number;
	 grid: Grid;
	 cells: Array<GridCell>;
	 draw(): void;
	 getCellAtPosition(col:number): GridCell;
	 getCells(): Array<GridCell>;
	 //new(index: number);
 }
 
 interface GridCell {
	
	index: number;
	lifeState: LifeState;
	neighbours: Array<GridCell>; 
	row: GridRow;
	
	//new(index: number);
	setLifeState(lifeState: LifeState): LifeState;	 
	setRandomLifeState();
 	toggleLifeState(): LifeState;
	draw(): void;
	getNeighbours(): void;
 }
 
 interface ConwaysGameOfLife {
	 step(): void;
 }
 
 interface Canvas {
	 ctx: any;
 }
 
 class GameGrid implements Grid {
	 size:number;
	 rows:Array<GridRow>;
	 cells:Array<GridCell>;
	 canvas:any;
	 private canvasId:string;
	 
	 
	 constructor(size:number, canvas:Canvas) {
		 this.size = size;
		 this.canvas = canvas;
		 this.rows = [];
		 this.cells = [];
		 
		 for (var rowNumber = 0; rowNumber < this.size; rowNumber++) {
			 this.rows[rowNumber] = <GridRow>new GameRow(rowNumber, this);
		 }
		 
		 var that = this;
		 for (var rowIndex in this.rows) {
			 this.rows[rowIndex].getCells().forEach(function(cell:GridCell) {
				cell.getNeighbours(); 
				that.cells.push(cell);
			 });
		 }
	 }
	 
	 debug() {
		 var debug = [];
		 
		 this.rows.forEach(function(row) {
			var tmpArray = [];
			row.cells.forEach(function(cell) {
				tmpArray.push(cell.lifeState === LifeState.Alive ? 1 : 0);
			});
			debug.push(tmpArray);
		 });
		 
		 console.log(debug);
	 }
	 
	 reinit() {
		 this.cells.forEach(function(cell:GridCell) {
			 cell.lifeState = LifeState.Dead;
		 });
	 }
	 
	 populate() {
		 this.cells.forEach(function(cell:GridCell) {
			 cell.setRandomLifeState();
		 });
	 }
	 
	 draw() {
		//console.log('Drawing Rows');
		this.canvas.ctx.clearRect(0, 0, this.size * GameCell.SIZE, this.size * GameCell.SIZE);
		
		for (var rowIndex in this.rows) {
			var row:GridRow = this.rows[rowIndex];
			
			row.draw();
		}
		
		return false;
	 }
	 
	 
	 
	 getCellAtPosition(col:number, row:number):GridCell {
		if ((col >= 0 && col <= this.size) && (row >= 0 && row <= this.size - 1)) {
			return this.rows[row].getCellAtPosition(col);	 
		}
		return null;
	 }
 }
 
 class GameRow implements GridRow {
	 index: number;
	 grid: Grid;
	 cells: Array<GridCell>;
	 
	 constructor(index:number, grid:Grid) {
		 this.index = index;
		 this.grid = grid;
		 this.cells = [];
		 
		 for (var cellNumber = 0; cellNumber < grid.size; cellNumber++) {
			 this.cells.push(new GameCell(cellNumber, this));
		 }
	 }
	 
	 draw() {
		 //console.log('Drawing cells');
		 
		 for (var cellIndex in this.cells) {
			var cell:GridCell = this.cells[cellIndex];
			cell.draw();
		}
	 }
	 
	 getCells() {
		 return this.cells;
	 }
	 
	 getCellAtPosition(col:number) {
		if (col >= 0 && col <= this.cells.length)
			return this.cells[col]; 
		return null;
	 }
 }
 
 class GameCell implements GridCell {
	 public static SIZE = 8;
	 
	 index: number;
	 lifeState: LifeState;
	 neighbours: Array<GridCell>;
	 totalLivingNeighbours: number;
	 row: GridRow;
	 
	 private static canvas: any;
	 private static CANVAS_ID = 'a_canvas';
	 
	 constructor (index: number, row: GridRow) {
		 this.index = index;
		 this.row = row;
		 this.neighbours = [];
		 this.totalLivingNeighbours = 0;
		 this.lifeState = LifeState.Dead;
	 }
	 
	 setRandomLifeState() {
		 this.lifeState = Math.random() > 0.5 ? this.setLifeState(LifeState.Alive) : this.setLifeState(LifeState.Dead);
	 }
	 
	 setLifeState(lifeState: LifeState) {
		 this.lifeState = lifeState;
		 return this.lifeState;
	 }
	 
	 toggleLifeState() {
		 this.lifeState = this.lifeState === LifeState.Alive ? LifeState.Dead : LifeState.Alive;
		 return this.lifeState;
	 }
	 
	 draw() {
		 //console.log('Drawing individual cell');
		 
		 if (this.lifeState === LifeState.Alive) {
			var gridSize = GameCell.SIZE;
			var xPos = this.index * gridSize;
			var yPos = this.row.index * gridSize;
			var grid = <GameGrid>this.row.grid;
			
			grid.canvas.ctx.fillRect(xPos, yPos, GameCell.SIZE, GameCell.SIZE);
		 }
	 }
	 
	 //TODO: Refactor
	 public getNeighbours() {
		 if(this.neighbours.length)
		 	return this.neighbours;
		 
		 var neighbours:Array<GridCell> = [];
		 var isFirstRow:boolean = false;
		 var isLastRow:boolean = false;
		 var isFirstCol:boolean = false;
		 var isLastCol:boolean = false;
		 var grid = this.row.grid;
		 var gridSize = grid.size;
		 
		 isFirstRow = this.row.index === 0 ? true : false;
		 isFirstCol = this.index === 0 ? true : false;
		 isLastRow = this.row.index === gridSize ? true : false;
		 isLastCol = this.index === gridSize ? true : false;
		 
		if (!isFirstRow) {	
			neighbours.push(grid.getCellAtPosition(this.index, this.row.index - 1))
			if (!isFirstCol)
				neighbours.push(grid.getCellAtPosition(this.index - 1, this.row.index - 1));
			if (!isLastCol)
				neighbours.push(grid.getCellAtPosition(this.index + 1, this.row.index - 1));
		}
		
		if (!isLastRow) {
			neighbours.push(grid.getCellAtPosition(this.index, this.row.index + 1));
			if (!isFirstCol)
				neighbours.push(grid.getCellAtPosition(this.index - 1, this.row.index + 1));
			if (!isLastCol)
				neighbours.push(grid.getCellAtPosition(this.index + 1, this.row.index + 1));
		}
		
		if (!isFirstCol)
				neighbours.push(grid.getCellAtPosition(this.index - 1, this.row.index));
		if (!isLastCol)
			neighbours.push(grid.getCellAtPosition(this.index + 1, this.row.index));
		 
		 this.neighbours = neighbours;
		 return neighbours;
	 }
 }
 
 class Game implements ConwaysGameOfLife {
	 private currentGen: GameGrid;
	 private nextGen: GameGrid;
	 private timerId:any;
	 private canvas:Canvas;
	 
	 constructor (size:number, canvasId:string, interval: number) {
		 this.canvas = new GameCanvas(canvasId, size);
		 this.currentGen = new GameGrid(size, this.canvas);
		 this.currentGen.populate();
		 this.nextGen = new GameGrid(size, this.canvas);
		 this.currentGen.draw();
	 }
	 
	 
	 step() {
		 var that = this;
		 this.currentGen.cells.forEach(function(cell:GridCell) {
			 var livingNeighbours:number = 0;
			 cell.neighbours.forEach(function(neighbour:GridCell) {
				 if (neighbour) {
				 	if (neighbour.lifeState === LifeState.Alive)
				 		livingNeighbours++;
				 }
			 });
			 
			 var nextGenCell:GridCell = that.nextGen.getCellAtPosition(cell.index, cell.row.index);
			 if (cell.lifeState === LifeState.Alive) {
				 nextGenCell.lifeState = LifeState.Alive;
				 
				if (livingNeighbours < 2)
					nextGenCell.lifeState = LifeState.Dead;
				if (livingNeighbours > 3)
					nextGenCell.lifeState = LifeState.Dead;
			 } else {
				 if (livingNeighbours == 3)
				 	nextGenCell.lifeState = LifeState.Alive;
			 }
		 });
		 
		 this.currentGen.debug();
		 //this.currentGen = this.nextGen;
		 
		 this.nextGen.cells.forEach(function(nCell) {
			 that.currentGen.rows[nCell.row.index].cells[nCell.index].lifeState = nCell.lifeState;
		 });
		 
		 //this.currentGen.canvas.ctx.clearRect(0, 0, 512, 512);
		 this.currentGen.draw();
		 this.nextGen.reinit();
	 }
 }
 
 class GameCanvas implements Canvas {
	 ctx: any;
	 
	 constructor(canvasId: string, gridSize: number) {
		 var canvas = <HTMLCanvasElement>document.getElementById(canvasId);
		 canvas.height = gridSize * GameCell.SIZE;
		 canvas.width = gridSize * GameCell.SIZE;
		 
		 this.ctx = canvas.getContext('2d');
	 }
 }
 
 var cgol = new Game(128, 'a_canvas', 0);
 
 setInterval(function() {
	  cgol.step();
  }, 100);